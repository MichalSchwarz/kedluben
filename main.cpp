
#include "Arduino.h"
#include "clock.h"
#include "my_wifi.h"
#include "my_server.h"

const int LIGHT_RELAY_PIN = 12;
const int UPDATE_PERIOD = 60000; // 1min
bool isLightEnabled = false;

bool isLightEnabledFn() {
  return isLightEnabled;
}

String getTimeFn() {
  String result = "";
  result += Clock::getHours();
  result += ":";
  result += Clock::getMinutes();
  result += ":";
  result += Clock::getSeconds();
  return result;
}

bool isLightTime() {
  bool result = false;
  if(Clock::getHours() > 8 && Clock::getHours() < 18) {
    result = true;
  }
  return result;
}

void setup() {
  pinMode(LIGHT_RELAY_PIN, OUTPUT);
  MyWifi::initialize();
  Clock::initialize( &MyWifi::getRealTime );
  MyServer::initialize(&isLightEnabledFn, &getTimeFn);
}

void loop() {
  if(isLightTime()) {
    digitalWrite(LIGHT_RELAY_PIN, HIGH);
    isLightEnabled = true;
  }
  else {
    digitalWrite(LIGHT_RELAY_PIN, LOW);
    isLightEnabled = false;
  }
  delay(UPDATE_PERIOD);
}
