#include "Arduino.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "my_wifi.h"
#include "secret.h"

namespace MyWifi {
  namespace {
    bool isReady = false;
    StaticJsonBuffer<1000> jsonBuffer;

    long parseTimeApi(String response) {
      JsonObject& root = jsonBuffer.parse(response);
      long time = root["timestamp"];
      return time;
    }

  }

  void initialize() {
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
    }
    isReady = true;
  }

  long getRealTime() {
    long result = 0;
    if(isReady) {
      HTTPClient http;
      http.begin(TIME_SYNC_URI);
      http.GET();
      String payload = http.getString();
      result = parseTimeApi(payload);
      http.end();
    }
    return result;
  }

}