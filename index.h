#ifndef Index_h
#define Index_h

#include "Arduino.h"

char index_html[] PROGMEM = " \
<!doctype html> \
<html lang=\"cs\"> \
<head> \
  <meta charset=\"utf-8\"> \
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> \
  <title>Kedluben</title> \
</head> \
<body> \
  Stav světla: ##LIGHT_STATUS## <br> \
  Čas: ##TIME## <br> \
</body> \
</html>";

#endif