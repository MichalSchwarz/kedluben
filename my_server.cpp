#include "Arduino.h"
#include "my_server.h"
#include "index.h"
#include "ESPAsyncWebServer.h"
#include <regex>

namespace MyServer {
  namespace {
    AsyncWebServer server(80);
    String lightStatus = "";
    String actualTime = "";
    bool (*isLightEnabledFn)();
    String (*getTimeFn)();

    std::string getResponse() {
      lightStatus = isLightEnabledFn() ? "Zapnuto" : "Vypnuto";
      actualTime = getTimeFn();
      std::string response = std::regex_replace(index_html, std::regex(R"(##LIGHT_STATUS##)"), lightStatus.c_str());
      response = std::regex_replace(response, std::regex(R"(##TIME##)"), actualTime.c_str());
      return response;
    }
  }

  void initialize( bool (*isLightEnabled)(), String (*getTime)() ) {
    isLightEnabledFn = isLightEnabled;
    getTimeFn = getTime;
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/html", getResponse().c_str());
    });
    server.begin();
  }
}