#ifndef MyServer_h
#define MyServer_h

#include "Arduino.h"

namespace MyServer {
  void initialize( bool (*isLightEnabled)(), String (*getTime)() );
};

#endif