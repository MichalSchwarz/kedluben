#ifndef MyWifi_h
#define MyWifi_h

#include "Arduino.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

namespace MyWifi {
  void initialize();
  long getRealTime();
};

#endif