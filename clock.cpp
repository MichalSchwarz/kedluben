#include "Arduino.h"
#include <Time.h>
#include "clock.h"

namespace Clock {

  void initialize(long (*timestamp)()) {
    setSyncProvider(timestamp);
    setSyncInterval(10);
    while(timeStatus() != timeSet) {
      delay(10000);
    }
    setSyncInterval(86400);
  }

  int getHours() {
    return hour();
  }

  int getMinutes() {
    return minute();
  }

  int getSeconds() {
    return second();
  }
}