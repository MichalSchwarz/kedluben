#ifndef Clock_h
#define Clock_h

#include "Arduino.h"

namespace Clock {
  void initialize(long());
  int getHours();
  int getMinutes();
  int getSeconds();
};

#endif